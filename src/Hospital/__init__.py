from flask import Flask
from flask.ext.login import LoginManager

app = Flask(__name__)
app.config["SECRET_KEY"] = "AppointmentSystemSecretKey"

login_manager = LoginManager()
login_manager.init_app(app)

import Hospital.views