# -*- coding: utf-8 -*-

sidebar_menu = {
  "nurse": [
    {
      "category": u"Pacienti",
      "links": [
        { "name": u"Pridať", "link": "/add_patient"},
        { "name": u"Zobraziť pacientov", "link": "/patients"},
        { "name": u"Zobraziť hospitalizácie", "link": "/hospitalizations" },
      ]
    },
  ],
  "doctor": [
    {
      "category": u"Pacienti",
      "links": [
        { "name": u"Pridať", "link": "/add_patient"},
        { "name": u"Zobraziť pacientov", "link": "/patients"},
        { "name": u"Zobraziť hospitalizácie", "link": "/hospitalizations" },
      ]
    },
  ],
  "admin": [
    {
      "category": u"Zamestnanci",
      "links": [
        { "name": u"Pridať", "link": "/add_employee" },
        { "name": u"Zobraziť", "link": "/employees" }
      ]
    },
    {
      "category": u"Lieky",
      "links": [
        { "name": u"Pridať", "link": "/add_medicament" },
        { "name": u"Zobraziť", "link": "/medicaments" }
      ]
    },
    {
      "category": u"Oddelenia",
      "links": [
        { "name": u"Pridať", "link": "/add_department" },
        { "name": u"Zobraziť", "link": "/departments" }
      ]
    },
  ]
}