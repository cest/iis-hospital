# -*- coding: utf-8 -*-

from Hospital import app, login_manager

from flask import Flask, render_template, flash, redirect, url_for, request, session
from flask.ext.login import current_user, login_user, logout_user
from flask.ext.security import login_required
from datetime import timedelta
import traceback
import sha
import time
from datetime import datetime
from functools import wraps

from database_decorators import create_db_connection
import config
from constants import *
from utils import *
from models import User

from pprint import pprint as pp

db_connection_read_only = create_db_connection(config.database)
db_connection = create_db_connection(config.database, read_only = False)

# decorator for routes
def restrict_route(allowed_users = []):
    def restriction(fun):
        @wraps(fun)
        def decorated_fun(*args, **kwargs):
            print "******* DECORATOR *********"
            print allowed_users
            print current_user.role
            print "******* DECORATOR *********"
            if allowed_users and current_user.role not in allowed_users:
                return redirect(url_for("error"))
            retval = fun(*args, **kwargs)
            return retval
        return decorated_fun
    return restriction


@login_manager.user_loader
def load_user(id):
    return User.get(id)

@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect(url_for("login"))

@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=10)

@app.errorhandler(404)
def page_not_found(error):
    return redirect(url_for("error"))

@app.route('/error', methods=["GET"])
@login_required
def error():
    return render_template("error.html", menu = sidebar_menu[current_user.role])

@app.route('/login', methods=["GET"])
def login():
    return render_template("login.html")

@app.route('/login/check', methods=["POST"])
def login_check():
    user = User.get(request.form["username"])
    if (user and user.password == sha.new(request.form["password"]).hexdigest()):
        login_user(user)
        return redirect(url_for("home"))
    else:
        flash("error-wrong-username-or-password")
        return redirect(url_for("login"))

@app.route('/logout', methods=["GET"])
def logout():
    logout_user()
    return redirect(url_for("login"))

@app.route('/profile', methods=["GET"])
@login_required
@db_connection_read_only
def show_profile(cur):
    cur.execute("SELECT * FROM employee WHERE  id = %s", (current_user.db_id, ))
    data = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
    data["date_of_birth"] = data["date_of_birth"].strftime("%d.%m.%Y")
    cur.execute("SELECT name FROM department WHERE id = %s", (data["department_id"],))
    dep = cur.fetchone()
    if dep:
        data["department"] = dep["name"]
    else:
        data["department"] = ""
    return render_template("profile.html", menu = sidebar_menu[current_user.role], data = data)

@app.route('/profile/password', methods=["POST"])
@login_required
@db_connection
def change_passwd(cur):
    new = request.form["new_password"]
    repeat = request.form["repeat_password"]
    if not new or not repeat:
        flash("fail-missing")
    elif new != repeat:
        flash("fail-not-equal")
    else:
        new_passw = sha.new(new).hexdigest()
        cur.execute("""UPDATE users
                       SET (password)
                        = (%s)
                        WHERE id = %s""", ( new_passw, current_user.db_id,))
        flash("success-password-changed")
    return redirect(url_for("show_profile"))

@app.route('/patients', methods=["GET"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection_read_only
def show_patients(cur):
    cur.execute("SELECT * FROM patient ORDER BY surname ASC")
    patient_data = []
    for patient in cur.fetchall():
        patient_data.append({ key:none2str(value) for key, value in patient.iteritems() })
        patient_data[-1]["date_of_birth"] = patient_data[-1]["date_of_birth"].strftime("%d.%m.%Y")
    return render_template("show_patients.html", menu = sidebar_menu[current_user.role], patients = patient_data, user = current_user.role)

@app.route('/patients/search', methods=["GET","POST"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection_read_only
def search_patients(cur):
    search = " AND ".join([ "%s = '%s'" % (key, value) for key, value in request.form.iteritems() if value ])
    if search:
        cur.execute("SELECT * FROM patient WHERE %s ORDER BY surname ASC" % search)
    else:
        cur.execute("SELECT * FROM patient ORDER BY surname ASC")
    patient_data = []
    for patient in cur.fetchall():
        patient_data.append({ key:none2str(value) for key, value in patient.iteritems() })
        patient_data[-1]["date_of_birth"] = patient_data[-1]["date_of_birth"].strftime("%d.%m.%Y")
    return render_template("patient_search.html", menu = sidebar_menu[current_user.role], patients = patient_data, post_data = request.form, user = current_user.role)

@app.route('/add_patient', methods=["GET"])
@login_required
@restrict_route(["doctor", "nurse"])
def add_patient():
    return render_template("add_patient.html", menu = sidebar_menu[current_user.role])

@app.route('/add_patient/finish', methods=["GET","POST"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection
def add_patient_finish(cur):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        cur.execute("""INSERT INTO patient (birth_number,
                                            name,
                                            surname,
                                            date_of_birth,
                                            gender,
                                            insurance, 
                                            address, 
                                            city,
                                            zip_code,
                                            phone,
                                            email) 
                        VALUES ( %(birth_number)s,
                                 %(name)s,
                                 %(surname)s,
                                 TO_DATE(%(date_of_birth)s, 'dd.mm.yyyy'),
                                 %(gender)s,
                                 %(insurance)s,
                                 %(address)s,
                                 %(city)s,
                                 %(zip_code)s,
                                 %(phone)s,
                                 %(email)s)""", data)
        flash("success-add-patient")
    except Exception as e:
        print traceback.format_exc(e)
        flash("fail-add-patient")
    return redirect(url_for("add_patient"))

@app.route('/employees/search', methods=["GET","POST"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def search_employees(cur):
    search = " AND ".join([ "%s = '%s'" % (key, value) for key, value in request.form.iteritems() if value ])
    employees_data = []
    if search:
        cur.execute("SELECT * FROM employee WHERE %s ORDER BY surname ASC" % search)
        for employee in cur.fetchall():
            employees_data.append({ key:none2str(value) for key, value in employee.iteritems() })
    else:
        cur.execute("SELECT * FROM employee ORDER BY surname ASC")
        for employee in cur.fetchall():
            employees_data.append({ key:none2str(value) for key, value in employee.iteritems() })
    return render_template("employees_search.html", menu = sidebar_menu[current_user.role], employees = employees_data,post_data = request.form)

@app.route('/add_employee', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def add_employee(cur):
    cur.execute("SELECT * FROM department")
    data = cur.fetchall()
    #pp(data)
    return render_template("add_employee.html", menu = sidebar_menu[current_user.role], departments = data)

@app.route('/add_employee/finish', methods=["GET","POST"])
@login_required
@restrict_route(["admin"])
@db_connection
def add_employee_finish(cur):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        #del(data["repeat_password"])
        data.setdefault("department_id", None)
        data.setdefault("competency", None)
        data.setdefault("degree", None)
        #pp(data)
        cur.execute("""INSERT INTO employee (birth_number,
                                            name,
                                            surname,
                                            date_of_birth,
                                            gender, 
                                            insurance, 
                                            address, 
                                            city,
                                            zip_code,
                                            phone,
                                            email,
                                            type,
                                            degree,
                                            competency,
                                            department_id) 
                        VALUES ( %(birth_number)s,
                                 %(name)s,
                                 %(surname)s,
                                 TO_DATE(%(date_of_birth)s, 'dd.mm.yyyy'),
                                 %(gender)s,
                                 %(insurance)s, 
                                 %(address)s,
                                 %(city)s,
                                 %(zip_code)s,
                                 %(phone)s,
                                 %(email)s,
                                 %(typ)s,
                                 %(degree)s,
                                 %(competency)s,
                                 %(department_id)s)""", data)

        data["password"] = sha.new(data["password"]).hexdigest()
        cur.execute(""" INSERT INTO users ( id,
                                            username,
                                            password)
                        VALUES ((SELECT last_value FROM employee_id_seq), 
                                %(username)s,
                                %(password)s)""", data)

        flash("success-add-employee")
    except Exception, e:
        print traceback.format_exc(e)
        flash("fail-add-employee")
    return redirect("add_employee")

@app.route('/add_department', methods=["GET"])
@login_required
@restrict_route(["admin"])
def add_department():
    return render_template("add_department.html", menu = sidebar_menu[current_user.role])

@app.route('/add_department/finish', methods=["GET","POST"])
@login_required
@restrict_route(["admin"])
@db_connection
def add_department_finish(cur):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        cur.execute("""INSERT INTO department (id,
                                            name,
                                            floor,
                                            phone) 
                        VALUES ( %(id)s,
                                 %(name)s,
                                 %(floor)s,
                                 %(phone)s)""", data)
        flash("success-add-department")
    except Exception, e:
        print traceback.format_exc(e)
        flash("fail-add-department")
    return redirect("add_department")

@app.route('/departments', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def show_departments(cur):
    cur.execute("SELECT * FROM department")
    return render_template("/show_departments.html", menu = sidebar_menu[current_user.role], departments = cur.fetchall())

@app.route('/department/<department_id>', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def show_department(cur ,department_id):
    cur.execute("SELECT * FROM department WHERE id = %s", (department_id, ))
    print cur.fetchone()
    return render_template("/department_profile.html", menu = sidebar_menu[current_user.role])

@app.route('/department/<department_id>/edit', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def edit_department(cur, department_id):
    cur.execute("SELECT * FROM department")
    data2 = cur.fetchall()
    try:
        cur.execute("SELECT * FROM department WHERE id = %s", (department_id, ))
        data = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("/edit_department.html", menu = sidebar_menu[current_user.role], department_data = data)

@app.route('/department/<department_id>/edit/finish', methods=["GET","POST"])
@login_required
@restrict_route(["admin"])
@db_connection
def edit_department_finish(cur, department_id):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data["id"] = department_id
        cur.execute("""UPDATE department
                       SET (id,
                            name,
                            floor,
                            phone) 
                        = ( %(id)s,
                            %(name)s,
                            %(floor)s,
                            %(phone)s)
                        WHERE id = %(id)s""", data)
        flash("success-edit-department")
    except Exception as e:
        print traceback.format_exc(e)
        flash("fail-edit-department")
        return redirect(url_for("edit_department", department_id = department_id))
    return redirect(url_for("show_departments", department_id = department_id))

@app.route('/patient/<patient_id>', methods=["GET"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection_read_only
def show_patient(cur, patient_id):
    try:
        cur.execute("SELECT * FROM patient WHERE id = %s", (patient_id, ))
        patient_data = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        cur.execute("""SELECT e.name, e.surname,
                              h.*,
                              d.name as department 
                       FROM hospitalization AS h,
                            employee AS e,
                            department AS d
                       WHERE h.patient_id = %s
                         AND h.department_id = d.id
                         AND h.employee_id = e.id
                       ORDER BY start_date DESC""", (patient_id, ))
        hospitalizations = []
        for h in cur.fetchall():
            h["start_date"] = h["start_date"].strftime("%d.%m.%Y")
            if h["end_date"]:
                h["end_date"] = h["end_date"].strftime("%d.%m.%Y")
            h["employee"] = "%s %s" % (h["name"], h["surname"])
            hospitalizations.append({ key:none2str(value) for key, value in h.iteritems() })

        cur.execute("""SELECT m.*, c.name, e.name as e_name, e.surname as e_surname
                       FROM prescribing_medicine AS m,
                             medicament AS c,
                             employee AS e
                       WHERE m.medicament_id = c.id 
                        AND e.id = m.employee_id
                        AND m.patient_id = %s""", (patient_id, ))
        medicaments = []
        for h in cur.fetchall():
            h["start_date"] = h["start_date"].strftime("%d.%m.%Y")
            h["employee"] = "%s %s" % (h["e_name"], h["e_surname"])
            medicaments.append({ key:none2str(value) for key, value in h.iteritems() })
        pp(medicaments)
        cur.execute("""SELECT a.*, e.name as e_name, e.surname as e_surname
                       FROM medical_arbitration AS a,
                             employee AS e
                       WHERE e.id = a.employee_id
                        AND a.patient_id = %s""", (patient_id, ))
        arbitrations = []
        for h in cur.fetchall():
            h["adate"] = h["adate"].strftime("%d.%m.%Y")
            h["employee"] = "%s %s" % (h["e_name"], h["e_surname"])
            arbitrations.append({ key:none2str(value) for key, value in h.iteritems() })
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("patient_profile.html", menu = sidebar_menu[current_user.role], patient_data = patient_data, arbitration_data = arbitrations, medicine = medicaments , hospitalizations = hospitalizations,  user = current_user.role)

@app.route('/patient/<patient_id>/edit', methods=["GET"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection_read_only
def edit_patient(cur, patient_id):
    try:
        cur.execute("SELECT * FROM patient WHERE id = %s", (patient_id, ))
        data = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        data["date_of_birth"] = data["date_of_birth"].strftime("%d.%m.%Y")
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("edit_patient.html", menu = sidebar_menu[current_user.role], patient_data = data)

@app.route('/patient/<patient_id>/hospitalization', methods=["GET"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection_read_only
def hospitalization(cur, patient_id):
    try:
        cur.execute("SELECT * FROM patient WHERE id = %s", (patient_id, ))
        patient = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        cur.execute("SELECT * FROM department")
        departments = cur.fetchall()
        # pp(request.form)
        hosp = {
            'diagnosis': '',
            'end_date': '',
            'room': '',
            'start_date': time.strftime("%d.%m.%Y"),
        }
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("add_hospitalization.html", menu = sidebar_menu[current_user.role], patient = patient, departments = departments, hospitalization = hosp)

@app.route('/patient/<patient_id>/hospitalization/finish', methods=["GET", "POST"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection
def hospitalization_finish(cur, patient_id):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data["patient_id"] = patient_id
        data["employee_id"] = current_user.db_id
        cur.execute("""INSERT INTO hospitalization (start_date,
                                            end_date,
                                            room, 
                                            diagnosis, 
                                            patient_id,
                                            department_id,
                                            employee_id) 
                        VALUES ( TO_DATE(%(start_date)s, 'dd.mm.yyyy'),
                                 TO_DATE(%(end_date)s, 'dd.mm.yyyy'),
                                 %(room)s, 
                                 %(diagnosis)s,
                                 %(patient_id)s,
                                 %(department_id)s,
                                 %(employee_id)s)""", data)
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("add_hospitalization", patient_id = patient_id))
    return redirect(url_for("show_patient", patient_id = patient_id))

@app.route('/hospitalizations', methods=["GET"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection_read_only
def show_hospitalizations(cur):
    cur.execute("""SELECT hospitalization.id, start_date, end_date, department.name as department,
                          patient.name as patient_name, patient.surname as patient_surname, patient.birth_number, 
                          employee.name as employee_name, employee.surname as employee_surname
                   FROM hospitalization, patient, department, employee
                   WHERE hospitalization.patient_id = patient.id 
                       AND hospitalization.department_id = department.id
                       AND hospitalization.employee_id = employee.id
                   ORDER BY start_date DESC""")
    hospitalization_data = []
    for hospitalization in cur.fetchall():
        hospitalization_data.append({ key:none2str(value) for key, value in hospitalization.iteritems() })
        hospitalization_data[-1]["start_date"] = hospitalization_data[-1]["start_date"].strftime("%d.%m.%Y")
        if hospitalization_data[-1]["end_date"]:
            hospitalization_data[-1]["end_date"] = hospitalization_data[-1]["end_date"].strftime("%d.%m.%Y")
        hospitalization_data[-1]["patient"] = "%s %s" % (hospitalization_data[-1]["patient_surname"], hospitalization_data[-1]["patient_name"])
        hospitalization_data[-1]["employee"] = "%s %s" % (hospitalization_data[-1]["employee_surname"], hospitalization_data[-1]["employee_name"])
    cur.execute("SELECT * FROM department")
    departments = cur.fetchall()
    return render_template("show_hospitalizations.html", menu = sidebar_menu[current_user.role], hospitalizations = hospitalization_data, departments = departments)

@app.route('/hospitalizations/search', methods=["GET","POST"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection_read_only
def search_hospitalizations(cur):
    search = ""
    for key, value in request.form.iteritems():
        if not value:
            continue
        if search:
            search += " AND "
        if key == "start_date":
            search += "%s >= TO_DATE('%s', 'dd.mm.yyyy')" % (key, value)
        elif key == "end_date":
            search += "%s <= TO_DATE('%s', 'dd.mm.yyyy')" % (key, value)
        else:
            search += "%s.id = '%s'" % (key, value)
    if search:
        search = "AND " + search
    cur.execute("""SELECT hospitalization.id, start_date, end_date, department.name as department,
                      patient.name as patient_name, patient.surname as patient_surname, patient.birth_number, 
                      employee.name as employee_name, employee.surname as employee_surname
               FROM hospitalization, patient, department, employee
               WHERE hospitalization.patient_id = patient.id 
                   AND hospitalization.department_id = department.id
                   AND hospitalization.employee_id = employee.id
                   %s
               ORDER BY start_date DESC""" % search)
    hospitalization_data = []
    for hospitalization in cur.fetchall():
        hospitalization_data.append({ key:none2str(value) for key, value in hospitalization.iteritems() })
        hospitalization_data[-1]["start_date"] = hospitalization_data[-1]["start_date"].strftime("%d.%m.%Y")
        if hospitalization_data[-1]["end_date"]:
            hospitalization_data[-1]["end_date"] = hospitalization_data[-1]["end_date"].strftime("%d.%m.%Y")
        hospitalization_data[-1]["patient"] = "%s %s" % (hospitalization_data[-1]["patient_surname"], hospitalization_data[-1]["patient_name"])
        hospitalization_data[-1]["employee"] = "%s %s" % (hospitalization_data[-1]["employee_surname"], hospitalization_data[-1]["employee_name"])
    cur.execute("SELECT * FROM department")
    departments = cur.fetchall()
    return render_template("hospitalization_search.html", menu = sidebar_menu[current_user.role], hospitalizations = hospitalization_data, departments = departments, post_data = request.form)

@app.route('/hospitalization/<hospitalization_id>/edit', methods=["GET"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection_read_only
def edit_hospitalization(cur, hospitalization_id):
    try:
        cur.execute("""SELECT p.name, p.surname, p.birth_number,
                              h.id, h.start_date, h.end_date, h.room, h.department_id, h.diagnosis
                       FROM hospitalization as h, patient as p
                       WHERE h.patient_id = p.id
                         AND h.id = %s""", (hospitalization_id, ))
        hospitalization = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        hospitalization["start_date"] = hospitalization["start_date"].strftime("%d.%m.%Y")
        if hospitalization["end_date"]:
            hospitalization["end_date"] = hospitalization["end_date"].strftime("%d.%m.%Y")
        cur.execute("SELECT * FROM department")
        departments = cur.fetchall()
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("edit_hospitalization.html", menu = sidebar_menu[current_user.role], hospitalization = hospitalization, departments = departments)

@app.route('/hospitalization/<hospitalization_id>/edit/finish', methods=["GET", "POST"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection
def edit_hospitalization_finish(cur, hospitalization_id):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data["id"] = hospitalization_id
        cur.execute("""UPDATE hospitalization 
                       SET (start_date,
                            end_date,
                            room, 
                            diagnosis, 
                            department_id) 
                        = ( TO_DATE(%(start_date)s, 'dd.mm.yyyy'),
                                 TO_DATE(%(end_date)s, 'dd.mm.yyyy'),
                                 %(room)s, 
                                 %(diagnosis)s,
                                 %(department_id)s)
                        WHERE id = %(id)s""", data)
    except Exception as e:
        print traceback.format_exc(e)
        # return redirect(url_for("add_hospitalization", patient_id = patient_id))
    return redirect(url_for("show_hospitalization", hospitalization_id = hospitalization_id))

@app.route('/hospitalization/<hospitalization_id>', methods=["GET"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection_read_only
def show_hospitalization(cur, hospitalization_id):
    try:
        cur.execute("""SELECT p.name as p_name, p.surname as p_surname, p.id as p_id ,p.birth_number, p.date_of_birth, p.insurance, p.gender,
                              e.name as e_name, e.surname as e_surname, e.type as e_type,
                              d.name as department,
                              h.id, h.start_date, h.end_date, h.room, h.department_id, h.diagnosis
                       FROM hospitalization as h, patient as p, employee as e, department as d
                       WHERE h.patient_id = p.id
                         AND h.employee_id = e.id
                         AND h.department_id = d.id
                         AND h.id = %s
                       ORDER BY start_date DESC""", (hospitalization_id, ))
        hospitalization = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        hospitalization["date_of_birth"] = hospitalization["date_of_birth"].strftime("%d.%m.%Y")
        hospitalization["start_date"] = hospitalization["start_date"].strftime("%d.%m.%Y")
        if hospitalization["end_date"]:
            hospitalization["end_date"] = hospitalization["end_date"].strftime("%d.%m.%Y")
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("hospitalization.html", menu = sidebar_menu[current_user.role] , hospitalization = hospitalization )

@app.route('/patient/<patient_id>/arbitration', methods=["GET"])
@login_required
@restrict_route(["doctor"])
@db_connection_read_only
def add_arbitration(cur, patient_id):
    try:
        cur.execute("SELECT * FROM patient WHERE id = %s", (patient_id, ))
        patient = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        arb = {
            'name': '',
            'details': '',
            'adate': time.strftime("%d.%m.%Y"),
        }
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("add_arbitration.html", menu = sidebar_menu[current_user.role], patient = patient, arb = arb)

@app.route('/patient/<patient_id>/arbitration/finish', methods=["GET", "POST"])
@login_required
@restrict_route(["doctor"])
@db_connection
def add_arbitration_finish(cur, patient_id):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data["patient_id"] = patient_id
        data["employee_id"] = current_user.db_id
        pp(data)
        cur.execute("""INSERT INTO medical_arbitration (adate,
                                            name,
                                            details,
                                            patient_id,
                                            employee_id) 
                        VALUES ( TO_DATE(%(adate)s, 'dd.mm.yyyy'),
                                 %(name)s,
                                 %(details)s,
                                 %(patient_id)s,
                                 %(employee_id)s)""", data)
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("add_arbitration", patient_id = patient_id))
    return redirect(url_for("show_patient", patient_id = patient_id))

@app.route('/arbitration/<arbitration_id>/edit', methods=["GET"])
@login_required
@restrict_route(["doctor"])
@db_connection_read_only
def edit_arbitration(cur, arbitration_id):
    try:
        cur.execute("""SELECT p.name, p.surname, p.birth_number, p.id as patient_id,
                              m.id, m.name, m.adate, m.details
                       FROM medical_arbitration as m, patient as p
                       WHERE m.patient_id = p.id
                         AND m.id = %s""", (arbitration_id, ))
        arbitration = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        arbitration["adate"] = arbitration["adate"].strftime("%d.%m.%Y")
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("edit_arbitration.html", menu = sidebar_menu[current_user.role], arbitration = arbitration)

@app.route('/arbitration/<arbitration_id>/edit/finish', methods=["GET", "POST"])
@login_required
@restrict_route(["doctor"])
@db_connection
def edit_arbitration_finish(cur, arbitration_id):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data["id"] = arbitration_id
        cur.execute("""UPDATE medical_arbitration 
                       SET (adate,
                            name,
                            details) 
                        = ( TO_DATE(%(adate)s, 'dd.mm.yyyy'),
                             %(name)s,
                             %(details)s)
                        WHERE id = %(id)s""", data)
    except Exception as e:
        print traceback.format_exc(e)
        # return redirect(url_for("add_hospitalization", patient_id = patient_id))
    return redirect(url_for("show_patient", patient_id = data["patient_id"]))


@app.route('/patient/<patient_id>/medicament', methods=["GET"])
@login_required
@restrict_route(["doctor"])
@db_connection_read_only
def prescribe_medicament(cur, patient_id):
    try:
        cur.execute("SELECT * FROM patient WHERE id = %s", (patient_id, ))
        patient = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        cur.execute("SELECT * FROM medicament WHERE active = 't'")
        medicaments = cur.fetchall()
        med = {
            'medicament_id': '',
            'specifications': '',
            'dosage': '',
            'amount': '',
            'start_date': time.strftime("%d.%m.%Y"),
        }
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("prescribe_medicament.html", menu = sidebar_menu[current_user.role], patient = patient, medicaments = medicaments, med = med)

@app.route('/patient/<patient_id>/medicament/finish', methods=["GET", "POST"])
@login_required
@restrict_route(["doctor"])
@db_connection
def prescribe_medicament_finish(cur, patient_id):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data["patient_id"] = patient_id
        data["employee_id"] = current_user.db_id
        cur.execute("""INSERT INTO prescribing_medicine (start_date,
                                            dosage,
                                            amount,
                                            specifications,
                                            patient_id,
                                            medicament_id,
                                            employee_id) 
                        VALUES ( TO_DATE(%(start_date)s, 'dd.mm.yyyy'),
                                 %(dosage)s, 
                                 %(amount)s,
                                 %(specifications)s,
                                 %(patient_id)s,
                                 %(medicament_id)s,
                                 %(employee_id)s)""", data)
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("prescribe_medicament", patient_id = patient_id))
    return redirect(url_for("show_patient", patient_id = patient_id))

@app.route('/medicament/<medicament_id>/edit', methods=["GET"])
@login_required
@restrict_route(["doctor"])
@db_connection_read_only
def edit_prescribe_medicament(cur, medicament_id):
    try:
        cur.execute("""SELECT p.name, p.surname, p.birth_number, p.id as patient_id,
                              m.id, m.medicament_id, m.start_date, m.dosage, m.amount, m.specifications
                       FROM prescribing_medicine as m, patient as p
                       WHERE m.patient_id = p.id
                         AND m.id = %s""", (medicament_id, ))
        prescribe_medicament = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        prescribe_medicament["start_date"] = prescribe_medicament["start_date"].strftime("%d.%m.%Y")
        cur.execute("SELECT * FROM medicament WHERE active = 't'")
        medicaments = cur.fetchall()
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("edit_prescribe_medicament.html", menu = sidebar_menu[current_user.role], medicament = prescribe_medicament, medicaments = medicaments)

@app.route('/medicament/<medicament_id>/edit/finish', methods=["GET", "POST"])
@login_required
@restrict_route(["doctor"])
@db_connection
def edit_prescribe_medicament_finish(cur, medicament_id):
    print "tutututu"
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data["id"] = medicament_id
        cur.execute("""UPDATE prescribing_medicine 
                       SET (start_date,
                            dosage,
                            amount, 
                            specifications, 
                            medicament_id) 
                        = ( TO_DATE(%(start_date)s, 'dd.mm.yyyy'),
                             %(dosage)s,
                             %(amount)s, 
                             %(specifications)s,
                             %(medicament_id)s)
                        WHERE id = %(id)s""", data)
    except Exception as e:
        print traceback.format_exc(e)
        # return redirect(url_for("add_hospitalization", patient_id = patient_id))
    return redirect(url_for("show_patient", patient_id = data["patient_id"]))

@app.route('/patient/<patient_id>/edit/finish', methods=["GET","POST"])
@login_required
@restrict_route(["doctor", "nurse"])
@db_connection
def edit_patient_finish(cur, patient_id):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data["id"] = patient_id
        cur.execute("""UPDATE patient
                       SET (birth_number,
                            name,
                            surname,
                            date_of_birth,
                            gender,
                            insurance, 
                            address, 
                            city,
                            zip_code,
                            phone,
                            email) 
                        = ( %(birth_number)s,
                            %(name)s,
                            %(surname)s,
                            TO_DATE(%(date_of_birth)s, 'dd.mm.yyyy'),
                            %(gender)s,
                            %(insurance)s, 
                            %(address)s,
                            %(city)s,
                            %(zip_code)s,
                            %(phone)s,
                            %(email)s)
                        WHERE id = %(id)s""", data)
        # flash("success-edit-patient")
    except Exception as e:
        print traceback.format_exc(e)
        # flash("fail-edit-patient")
        return redirect(url_for("edit_patient", patient_id = patient_id))
    return redirect(url_for("show_patient", patient_id = patient_id))

@app.route('/employees', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def show_employees(cur):
    cur.execute("SELECT * FROM employee")
    employees_data = []
    for employee in cur.fetchall():
        employees_data.append({ key:none2str(value) for key, value in employee.iteritems() })
    return render_template("/show_employees.html", menu = sidebar_menu[current_user.role], employees = employees_data)

@app.route('/employee/<employee_id>', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def show_employee(cur,employee_id):
    try:
        cur.execute("SELECT * FROM employee WHERE id = %s", (employee_id, ))
        employee_data = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("employee_profile.html", menu = sidebar_menu[current_user.role], employee_data = employee_data )

@app.route('/employee/<employee_id>/edit', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def edit_employee(cur, employee_id):
    cur.execute("SELECT * FROM department")
    data2 = cur.fetchall()
    try:
        cur.execute("SELECT * FROM employee WHERE id = %s", (employee_id, ))
        data = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
        pp(data)
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("/edit_employee.html", menu = sidebar_menu[current_user.role], employee_data = data, departments = data2)


@app.route('/employee/<employee_id>/edit/finish', methods=["GET","POST"])
@login_required
@restrict_route(["admin"])
@db_connection
def edit_employee_finish(cur, employee_id):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data.setdefault("department_id", None)
        data.setdefault("competency", None)
        data.setdefault("degree", None)
        data["id"] = employee_id
        #pp(data)
        cur.execute("""UPDATE employee
                       SET (birth_number,
                            name,
                            surname,
                            date_of_birth, 
                            insurance, 
                            address, 
                            city,
                            zip_code,
                            phone,
                            email,
                            type,
                            degree,
                            competency,
                            department_id,
                            active) 
                        = ( %(birth_number)s,
                            %(name)s,
                            %(surname)s,
                            TO_DATE(%(date_of_birth)s, 'dd.mm.yyyy'),
                            %(insurance)s, 
                            %(address)s,
                            %(city)s,
                            %(zip_code)s,
                            %(phone)s,
                            %(email)s,
                            %(typ)s,
                            %(degree)s,
                            %(competency)s,
                            %(department_id)s,
                            %(active)s)
                        WHERE id = %(id)s""", data)
        flash("success-edit-employee")
    except Exception as e:
        print traceback.format_exc(e)
        flash("fail-edit-employee")
        return redirect(url_for("edit_employee", employee_id = employee_id))
    return redirect(url_for("show_employees", employee_id = employee_id))

@app.route('/medicaments', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def show_medicaments(cur):
    cur.execute("SELECT * FROM medicament")
    return render_template("show_medicament.html", menu = sidebar_menu[current_user.role], medicaments = cur.fetchall())

@app.route('/add_medicament', methods=["GET"])
@login_required
@restrict_route(["admin"])
def add_medicament():
    return render_template("add_medicament.html", menu = sidebar_menu[current_user.role])

@app.route('/add_medicament/finish', methods=["GET","POST"])
@login_required
@restrict_route(["admin"])
@db_connection
def add_medicament_finish(cur):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        cur.execute("""INSERT INTO medicament (name,
                                            active_substance,
                                            composition, 
                                            indications, 
                                            contraindications,
                                            form,
                                            rec_dosage) 
                        VALUES ( %(name)s,
                                 %(active_substance)s,
                                 %(composition)s, 
                                 %(indications)s,
                                 %(contraindications)s,
                                 %(form)s,
                                 %(rec_dosage)s)""", data)
        flash("success-add-medicament")
    except Exception as e:
        print traceback.format_exc(e)
        flash("fail-add-medicament")
    return redirect(url_for("add_medicament"))


@app.route('/medicament/<medicament_id>', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def show_medicament(cur ,medicament_id):
    cur.execute("SELECT * FROM medicament WHERE id = %s", (medicament_id, ))
    print cur.fetchone()
    return render_template("/medicament_profile.html", menu = sidebar_menu[current_user.role])

@app.route('/medicament/<medicament_id>/edit', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def edit_medicament(cur, medicament_id):
    try:
        cur.execute("SELECT * FROM medicament WHERE id = %s", (medicament_id, ))
        data = { key:none2str(value) for key, value in cur.fetchone().iteritems() }
    except Exception as e:
        print traceback.format_exc(e)
        return redirect(url_for("error"))
    return render_template("/edit_medicament.html", menu = sidebar_menu[current_user.role], medicament_data = data)

@app.route('/medicament/<medicament_id>/edit/finish', methods=["GET","POST"])
@login_required
@restrict_route(["admin"])
@db_connection
def edit_medicament_finish(cur, medicament_id):
    try:
        data = { key:str2none(value) for key, value in request.form.iteritems() }
        data["id"] = medicament_id
        cur.execute("""UPDATE medicament
                       SET (name,
                            active_substance,
                            composition, 
                            indications, 
                            contraindications, 
                            form,
                            rec_dosage,
                            active)
                        = ( %(name)s,
                            %(active_substance)s,
                            %(composition)s,
                            %(indications)s, 
                            %(contraindications)s,
                            %(form)s,
                            %(rec_dosage)s,
                            %(active)s)
                        WHERE id = %(id)s""", data)
        flash("success-edit-medicament")
    except Exception as e:
        print traceback.format_exc(e)
        flash("fail-edit-medicament")
        return redirect(url_for("edit_medicament", medicament_id = medicament_id))
    return redirect(url_for("show_medicaments", medicament_id = medicament_id))


@app.route('/medicaments/search', methods=["GET","POST"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def search_medicaments(cur):
    search = " AND ".join([ "%s = '%s'" % (key, value) for key, value in request.form.iteritems() if value ])
    medicaments_data = []
    if search:
        cur.execute("SELECT * FROM medicament WHERE %s ORDER BY name ASC" % search)
        for medicament in cur.fetchall():
            medicaments_data.append({ key:none2str(value) for key, value in medicament.iteritems() })
    else:
        cur.execute("SELECT * FROM medicament ORDER BY name ASC")
        for medicament in cur.fetchall():
            medicaments_data.append({ key:none2str(value) for key, value in medicament.iteritems() })
    return render_template("medicaments_search.html", menu = sidebar_menu[current_user.role], medicaments = medicaments_data,post_data = request.form)

@app.route('/', methods=["GET"])
@login_required
@db_connection_read_only
def home(cur):
    if current_user.role == "admin":
        return redirect(url_for("admin_home"))
    cur.execute("""SELECT hospitalization.id, start_date, end_date, department.name as department,
                          patient.name as patient_name, patient.surname as patient_surname, patient.birth_number, 
                          employee.name as employee_name, employee.surname as employee_surname
                   FROM hospitalization, patient, department, employee
                   WHERE hospitalization.patient_id = patient.id 
                       AND hospitalization.department_id = department.id
                       AND hospitalization.employee_id = employee.id
                   ORDER BY id DESC
                   LIMIT 5""")
    hospitalization_data = []
    for hospitalization in cur.fetchall():
        hospitalization_data.append({ key:none2str(value) for key, value in hospitalization.iteritems() })
        hospitalization_data[-1]["start_date"] = hospitalization_data[-1]["start_date"].strftime("%d.%m.%Y")
        if hospitalization_data[-1]["end_date"]:
            hospitalization_data[-1]["end_date"] = hospitalization_data[-1]["end_date"].strftime("%d.%m.%Y")
        hospitalization_data[-1]["patient"] = "%s %s" % (hospitalization_data[-1]["patient_surname"], hospitalization_data[-1]["patient_name"])
        hospitalization_data[-1]["employee"] = "%s %s" % (hospitalization_data[-1]["employee_surname"], hospitalization_data[-1]["employee_name"])

    cur.execute("SELECT * FROM patient ORDER BY id DESC LIMIT 5")
    patient_data = []
    for patient in cur.fetchall():
        patient_data.append({ key:none2str(value) for key, value in patient.iteritems() })
        patient_data[-1]["date_of_birth"] = patient_data[-1]["date_of_birth"].strftime("%d.%m.%Y")

    cur.execute("SELECT COUNT(*) FROM hospitalization WHERE end_date IS NULL")
    current_hosp = cur.fetchone()["count"]
    print current_hosp
    cur.execute("SELECT COUNT(*) FROM employee")
    current_emp = cur.fetchone()["count"]
    print current_emp
    cur.execute("SELECT COUNT(*) FROM patient")
    current_pat = cur.fetchone()["count"]
    print current_pat
    return render_template("index.html", menu = sidebar_menu[current_user.role], hosp = current_hosp, emp = current_emp, pat = current_pat, hospitalizations = hospitalization_data, patients = patient_data)

@app.route('/admin', methods=["GET"])
@login_required
@restrict_route(["admin"])
@db_connection_read_only
def admin_home(cur):
    cur.execute("SELECT * FROM employee ORDER BY id DESC LIMIT 5")
    employee_data = []
    for employee in cur.fetchall():
        employee_data.append({ key:none2str(value) for key, value in employee.iteritems() })
        employee_data[-1]["date_of_birth"] = employee_data[-1]["date_of_birth"].strftime("%d.%m.%Y")
        cur.execute("SELECT name FROM department WHERE id = %s", (employee_data[-1]["department_id"],))
        dep = cur.fetchone()
        if dep:
            employee_data[-1]["department"] = dep["name"]
        else:
            employee_data[-1]["department"] = ""

    cur.execute("SELECT COUNT(*) FROM employee")
    current_emp = cur.fetchone()["count"]
    print current_emp
    cur.execute("SELECT COUNT(*) FROM medicament")
    current_med = cur.fetchone()["count"]
    print current_med
    cur.execute("SELECT COUNT(*) FROM department")
    current_dep = cur.fetchone()["count"]
    print current_dep
    return render_template("admin_index.html", menu = sidebar_menu[current_user.role], dep = current_dep, emp = current_emp, med = current_med, employees = employee_data)