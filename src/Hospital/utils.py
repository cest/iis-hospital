# -*- coding: utf-8 -*-

def str2none(string):
    if string == "":
        return None
    else:
        return string

def none2str(data):
    if data:
        return data
    else:
        return ""