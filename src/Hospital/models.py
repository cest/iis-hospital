# -*- coding: utf-8 -*-
from flask.ext.login import UserMixin
import config

from database_decorators import *
db_connection = create_db_connection(config.database)

class UserNotFoundError(Exception):
    pass


class User(UserMixin):

    def __init__(self, id):
        user = self.load(id)
        if not user:
            raise UserNotFoundError()
        self.id = user["username"] # username
        self.password = user["password"]
        self.db_id = user["id"] # id in DB
        if user["type"] == u"Lekár":
            self.role = "doctor"
        elif user["type"] == u"Sestra":
            self.role = "nurse"
        else:
            self.role = "admin"

    @db_connection
    def load(cur, self, id):
        cur.execute("SELECT * FROM users, employee WHERE users.id = employee.id AND username = %s AND employee.active = 't'", (id,))
        user = cur.fetchone()
        return user

    @classmethod
    def get(self_class, id):
        try:
            return self_class(id)
        except UserNotFoundError:
            return None
