from functools import wraps
import psycopg2
import psycopg2.extras
import psycopg2.extensions
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

def create_db_connection(conn_conf, read_only=True):
    def db_connection(fun):
        @wraps(fun)
        def decorated_fun(*args, **kwargs):
            conn = psycopg2.connect(conn_conf)
            cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
            retval = fun(cur, *args, **kwargs)
            if not read_only:
                conn.commit()
            return retval
        return decorated_fun
    return db_connection
