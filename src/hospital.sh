#! /bin/bash
# /etc/init.d/uwsgi
#

source /srv/venv/bin/activate
daemon=/srv/venv/bin/uwsgi
pid=/tmp/hospital.pid
cd /srv/iis-hospital/src
args="--ini /srv/iis-hospital/src/hospital.ini"

case "$1" in
   start)
       echo "Starting uwsgi"
       start-stop-daemon -p $pid --start --exec $daemon -- $args
       ;;
   stop)
       echo "Stopping script uwsgi"
       start-stop-daemon --signal INT -p $pid --stop $daemon -- $args
       ;;
   *)
       echo "Usage: /etc/init.d/hospital.sh {start|stop}"
       exit 1
   ;;
esac

exit 0
