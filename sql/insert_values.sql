
INSERT INTO patient(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email)
    VALUES (
        '815808/6981',
        'Jozef',
        'Pekný',
        TO_DATE('13.5.1976', 'dd.mm.yyyy'),
        'Muž',
        'Union',
        'Hlboká 30',
        'Piešťany',
        '92101',
        '0947148552',
        'jozopek@gmail.com'
);

INSERT INTO patient(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone)
    VALUES (
        '931126/1487',
        'Dušan',
        'Kolník',
        TO_DATE('1.7.1950', 'dd.mm.yyyy'),
        'Muž',
        'Dôvera',
        'A.Trajana 21',
        'Piešťany',
        '92101',
        '0912228597'
);

INSERT INTO patient(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email)
    VALUES (
        '830422/4511',
        'František',
        'Grgol',
        TO_DATE('22.4.1983', 'dd.mm.yyyy'),
        'Muž',
        'VŠZP',
        'Brezová 45',
        'Piešťany',
        '92101',
        '0989711298',
        'grgo.fero@centrum.sk'
);

INSERT INTO patient(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email)
    VALUES (
        '900207/7711',
        'Jozef',
        'Gogovič',
        TO_DATE('7.2.1990', 'dd.mm.yyyy'),
        'Muž',
        'VŠZP',
        'Dlhá 8',
        'Piešťany',
        '92101',
        '0905784217',
        'jozef@gogovic.com'
);

INSERT INTO patient(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email)
    VALUES (
        '770624/0641',
        'Ján',
        'Dubnička',
        TO_DATE('15.2.1988', 'dd.mm.yyyy'),
        'Muž',
        'VŠZP',
        'Pod Párovcami 122',
        'Piešťany',
        '92101',
        '0911268447',
        'dubnicka.jan@gmail.com'
);


INSERT INTO department(id, name, floor, phone)
    VALUES (
        'CHI',
        'Chirurgia',
        1,
        '100'
);

INSERT INTO department(id, name, floor, phone)
    VALUES (
        'PED',
        'Pediatria',
        2,
        '200'
);

INSERT INTO department(id, name, floor, phone)
    VALUES (
        'ONK',
        'Onkológia',
        1,
        '103'
);

INSERT INTO department(id, name, floor, phone)
    VALUES (
        'ORT',
        'Ortopédia',
        1,
        '101'
);



INSERT INTO employee(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email, type, degree, competency, department_id)
    VALUES (
        '820505/2790',
        'Edmund',
        'Šarkózy',
        TO_DATE('5.5.1982', 'dd.mm.yyyy'),
        'Muž',
        'VŠZP',
        'Brezová 22',
        'Šaľa',
        '92701',
        '0911222147',
        'sarkozy@gmail.com',
        'Lekár',
        'Mudr.',
        'Chirurg so zameraním na dolné končatiny.',
        'CHI'
);


INSERT INTO employee(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email, type, department_id)
    VALUES (
        '815110/5798',
        'Mária',
        'Belicová',
        TO_DATE('10.1.1981', 'dd.mm.yyyy'),
        'Žena',
        'VŠZP',
        'Brezová 22',
        'Vrbové',
        '92203',
        '0911243434322',
        'maria.belic@gmail.com',
        'Sestra',
        'CHI'
);


INSERT INTO employee(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email, type)
    VALUES (
        '931116/6735',
        'Martin',
        'Černek',
        TO_DATE('16.11.1993', 'dd.mm.yyyy'),
        'Muž',
        'Dôvera',
        'Družby 54',
        'Piešťany',
        '92101',
        '0915435746',
        'cernekm1@gmail.com',
        'Administrátor'
);


INSERT INTO employee(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email, type, degree, competency, department_id)
    VALUES (
        '870512/5231',
        'Rastislav',
        'Horváth',
        TO_DATE('12.5.1987', 'dd.mm.yyyy'),
        'Muž',
        'Union',
        'Mierová 14',
        'Bernolákovo',
        '90027',
        '0907411579',
        'horvath@gmail.com',
        'Lekár',
        'Mudr.',
        'Ortopéd',
        'ORT'
);

INSERT INTO employee(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email, type, degree, competency, department_id)
    VALUES (
        '876101/5879',
        'Ján',
        'Kováč',
        TO_DATE('1.11.1987', 'dd.mm.yyyy'),
        'Muž',
        'Dôvera',
        'Krížna 4',
        'Stropkov',
        '90741',
        '0901821269',
        'kovac@gmail.com',
        'Lekár',
        'Mudr.',
        'Onkológ',
        'ONK'
);

INSERT INTO employee(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email, type, degree, competency, department_id)
    VALUES (
        '856101/5879',
        'Margita',
        'Hlavatá',
        TO_DATE('1.11.1985', 'dd.mm.yyyy'),
        'Žena',
        'Union',
        'Okružná 4',
        'Trnava',
        '92243',
        '0904771382',
        'hlavata@gmail.com',
        'Lekár',
        'Mudr.',
        'Pediater',
        'PED'
);

INSERT INTO employee(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email, type, department_id)
    VALUES (
        '755208/9400',
        'Helena',
        'Majerníková',
        TO_DATE('8.2.1975', 'dd.mm.yyyy'),
        'Žena',
        'Union',
        'Vážska 7',
        'Šenkvice',
        '92705',
        '0904771382',
        'majernikova@gmail.com',
        'Sestra',
        'PED'
);

INSERT INTO employee(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email, type, department_id)
    VALUES (
        '875410/8622',
        'Jana',
        'Slabá',
        TO_DATE('10.4.1987', 'dd.mm.yyyy'),
        'Žena',
        'VŠZP',
        'Javorová 42',
        'Piešťany',
        '92101',
        '0908452249',
        'slaba@gmail.com',
        'Sestra',
        'ONK'
);

INSERT INTO employee(birth_number, name, surname, date_of_birth, gender, insurance, address, city, zip_code, phone, email, type, department_id)
    VALUES (
        '850512/8622',
        'Miloslava',
        'Kostadinová',
        TO_DATE('12.5.1985', 'dd.mm.yyyy'),
        'Žena',
        'Dôvera',
        'Winterova 8',
        'Piešťany',
        '92101',
        '0949771620',
        'kostadinova@gmail.com',
        'Sestra',
        'ORT'
);

INSERT INTO users(id, username, password)
    VALUES (
        1,
        'edo',
        '9da14ce833f5a7b709513cc8f6de983d5ea2bd91' -- edo
);

INSERT INTO users(id, username, password)
    VALUES (
        2,
        'belicova',
        '15a16b07de1ae925e8400473ba99d2ce11d90028' -- belicova
);

INSERT INTO users(id, username, password)
    VALUES (
        3,
        'martas',
        '41e0aea28369f852cc68498257b11cb859f22d56' -- martas
);

INSERT INTO users(id, username, password)
    VALUES (
        4,
        'rasto',
        'cbd678532c5caf4da350c4eb052aba98e005fb34' -- rasto
);

INSERT INTO users(id, username, password)
    VALUES (
        5,
        'jano',
        'b07fe6fe542d2d3b400e59b6e08eab04901148be' -- jano
);

INSERT INTO users(id, username, password)
    VALUES (
        6,
        'margit',
        '75486df41c55128fe408a5bdb80201ec9ebfc22b' -- margit
);

INSERT INTO users(id, username, password)
    VALUES (
        7,
        'hela',
        '069d836d340faec8eb5b5d6689352e19e02336c3' -- hela
);

INSERT INTO users(id, username, password)
    VALUES (
        8,
        'jana',
        'a2468f28b1edfae3e0a0ca3842941d5601441434' -- jana
);

INSERT INTO users(id, username, password)
    VALUES (
        9,
        'milka',
        '2aeb4900d607f7bbf9653c84584fbe097ddf3c81' -- milka
);



INSERT INTO medicament(name,active_substance,composition,indications,contraindications,form,rec_dosage)
    VALUES (
        'Paralen 500',
        'Paracetamolum 500 mg',
        'Paracetamolum,kukuričný škrob predželatínovaný,povidón 30,sodná soľ kroskarmelózy,kyselina stearová',
        'Horúčka,bolesti zubov,hlavy,neuralgie,bolesti svalov alebo kĺbov nezápalovej etiológie,bolesti vertebrogénneho pôvodu,bolestivá menštruácia',
        'Precitlivenosť na liečivo alebo na ktorúkoľvek z pomocných látok,ťažká hemolytická anémia,ťažké formy hepatálnej insuficiencie,akútna hepatitída',
        'Tablety',
        'Dospelým a dospievajúcim sa podáva 1-2 tablety podľa potreby v časovom odstupe najmenej 4 hodiny do maximálnej dennej dávky 4g. Deťom a dospievajúcim sa podáva 1/2 - 1 tableta Paralenu 500,3 -krát denne.'
);


INSERT INTO medicament(name,active_substance,composition,indications,contraindications,form,rec_dosage)
    VALUES (
        'Tebokan 40 mg',
        '40 mg suchého extraktu z listov Ginkgo biloba (35 – 67 : 1)',
        'Monohydrát laktózy,mikrokryštalická celulóza,kukuričný škrob,koloidný oxid kremičitý,sodná soľ kroskarmelózy,magnéziumstearát,hydroxypropylmetylcelulóza,makrogol 1500,oxid titaničitý E 171,žltý oxid železitý E 172,mastenec,dimetikón,makrogolstearyléter',
        'Poruchy mozgovej výkonnosti vyvolané nedostatočným prietokom krvi mozgom ako aj jeho nedostatočné zásobenie kyslíkom a živinami s príznakmi zníženej intelektuálnej výkonnosti,závratmi,hučaním v ušiach,bolesťami hlavy,zhoršeným videním,poruchami pamäti.',
        'Precitlivenosť na liečivo alebo na ktorúkoľvek z pomocných látok. V takomto prípade sa liek nesmie užívať.',
        'Tablety',
        'Počiatočná liečba:3-krát denne 1 tabletu počas 8 - 12 týždňov, Dlhodobá liečba:2-krát denne 1 tabletu, Pri dementnom syndróme:3-krát denne 1-2 tablety.'
);

INSERT INTO medicament(name,active_substance,composition,indications,contraindications,form,rec_dosage)
    VALUES (
        'Zenaro 5 mg',
        '5 mg levocetirizín dihydrochloridu,monohydrát laktózy, 67,5 mg v každej tablete',
        'Jadro tablety: monohydrát laktózy,mikrokryštalická celulóza,glykolát sodného škrobu (typ A),koloidný bezvodý oxid kremičitý,magnéziumstearát, Filmotvorná vrstva:hypromelóza 2910/5,makrogol 6000,mastenec,oxid titaničitý (E171)',
        'Symptomatická liečba alergickej rinitídy (vrátane perzistujúcej alergickej rinitídy) a urtikárie',
        'Precitlivenosť na liečivo, na iné piperazínové deriváty alebo na ktorúkoľvek z pomocných látok. Ťažká porucha funkcie obličiek s hodnotou klírensu kreatinínu menej ako 10 ml/min',
        'Filmom obalené tablety',
        'Dospelí a mladiství od 12 rokov: Odporúčaná denná dávka je 5 mg (1 filmom obalená tableta). Deti vo veku 6 až 12 rokov: Odporúčaná denná dávka je 5 mg (1 filmom obalená tableta)'
);


INSERT INTO medicament(name,active_substance,composition,indications,contraindications,form,rec_dosage)
    VALUES (
        'Ibalgin 400',
        'Ibuprofen 400 mg v 1 filmom obalenej tablete',
        'Maydis amylum, maydis amylum pregelificatum, carboxymethylamylum natricum C, acidum stearicum, talcum, silica colloidalis anhydrica, hypromellosum, macrogolum, titanii dioxidum E 171, erythrosinum E 127, simeticoni emulsio SE 4',
        'Ibuprofen je indikovaný na liečbu zápalových a degeneratívnych ochorení kĺbov, mimokĺbového reumatizmu a ochorení chrbtice',
        'Precitlivenosť na liečivo alebo na ktorúkoľvek z pomocných látok alebo precitlivenosť na kyselinu acetylsalicylovú a iné nesteroidové antiflogistiká prejavujúca sa ako astma, urtikária a iné alergické reakcie',
        'Filmom obalené tablety',
        'Dospelí a dospievajúci od 12 rokov: Dávkovanie je v rozsahu 1,2 - 2,4 g denne, (zvyčajne 1,2 - 1,8 g denne) rozdelené podľa závažnosti ochorenia a reakcie pacienta na liečbu. Dávka 2,4 g denne sa nemá prekročiť'
);


INSERT INTO medicament(name,active_substance,composition,indications,contraindications,form,rec_dosage)
    VALUES (
        'Ditustat',
        'Dropropizín 220 mg v 10 ml roztoku',
        'Anízová silica,Silica mäty piepornej,Polysorbát 80,Monohydrát kyseliny citrónovej,Monohydrát sodnej soli sacharínu,Kyselina askorbová,Čistená voda',
        'Dráždivý kašeľ pri faryngitíte, laryngitíde, tracheitíde, akútnej a chronickej bronchitíde, pneumotoraxe, bronchiektázach a pri hemoptoe rôznej etiológie',
        'Liek sa nesmie podávať pacientom s precitlivenosťou na dropropizín,pacientom s ochorením spojeným s bronchiálnou hypersekréciou alebo narušením mukociliárneho aparátu.Liek je kontraindikovaný u gravidných a dojčiacich žien a u detí do 6 mesiacov',
        'Perorálne roztokové kvapky',
        'Deťom vo veku 6 – 12 mesiacov 6 kvapiek 3-krát denne, vo veku 1 – 3 roky 13 kvapiek 3-krát denne, vo veku 3 – 13 rokov 26 kvapiek 3-krát denne, dospievajúcim a dospelým 52 kvapiek 3-krát denne alebo 25 kvapiek 6-krát denne'
);


INSERT INTO medicament(name,active_substance,composition,indications,contraindications,form,rec_dosage)
    VALUES (
        'Flavamed forte perorálny roztok',
        '5 ml perorálneho roztoku obsahuje 30 mg ambroxoliumchloridu,sorbitol 1,75 g/5 ml',
        'Sorbitol (E 420), tekutý (nekryštalický),Kyselina benzoová (E 210),Glycerol,Hydroxyetylcelulóza,Koncentrát s malinovou príchuťou,Purifikovaná voda',
        'Mukolytická liečba produktívneho kašľa, ktorý sprevádza akútne a chronické bronchopulmonálne ochorenia',
        'Precitlivelosť na liečivo alebo ktorúkoľvek z pomocných látok. Flavamed forte perorálny roztok sa nesmie používať u detí do dvoch rokov.',
        'Perorálny roztok',
        'Deti od 2 do 5 rokov: 1/4 odmernej lyžičky zodpovedá 1,25 ml perorálneho roztoku 3x denne, Dospelí a adolescenti starší ako 12 rokov: 1 odmerná lyžička obsahujúca 5 ml perorálneho roztoku sa užíva 3x denne'
);


INSERT INTO hospitalization(start_date,end_date,room,diagnosis,patient_id,department_id,employee_id)
    VALUES (
        TO_DATE('10.7.2015', 'dd.mm.yyyy'),
        TO_DATE('15.7.2015', 'dd.mm.yyyy'),
        '117',
        'Zlomenina stehennej kosti, pohmliaždeniny',
        2,
        'ORT',
        4
);

INSERT INTO hospitalization(start_date,end_date,room,diagnosis,patient_id,department_id,employee_id)
    VALUES (
        TO_DATE('14.10.2015', 'dd.mm.yyyy'),
        TO_DATE('16.10.2015', 'dd.mm.yyyy'),
        '204',
        'Horúčky, dehydradácia, kolaps organizmu',
        4,
        'PED',
        6
);

INSERT INTO hospitalization(start_date,end_date,room,diagnosis,patient_id,department_id,employee_id)
    VALUES (
        TO_DATE('29.8.2015', 'dd.mm.yyyy'),
        TO_DATE('10.9.2015', 'dd.mm.yyyy'),
        '308',
        'Plastická operácia nosa',
        5,
        'CHI',
        1
);


INSERT INTO medical_arbitration(name,adate,details,patient_id,employee_id)
    VALUES (
        'Rontgenové vyšetrenie',
        TO_DATE('11.7.2015', 'dd.mm.yyyy'),
        'Rontgenové pravej nohy, kvôli potvrdeniu, resp. vylúčeniu zlomeniny',
        2,
        4
);

INSERT INTO medical_arbitration(name,adate,details,patient_id,employee_id)
    VALUES (
        'Podanie infúzii',
        TO_DATE('14.10.2015', 'dd.mm.yyyy'),
        'Ṕodanie 500 ml infúzii',
        4,
        6
);

INSERT INTO medical_arbitration(name,adate,details,patient_id,employee_id)
    VALUES (
        'Predoperačné vyšetrenie',
        TO_DATE('30.8.2015', 'dd.mm.yyyy'),
        'Predoperačné vyšetrenie kvôli zisteniu spôsobilosti na vykonanie operačného zákroku',
        5,
        1
);

INSERT INTO prescribing_medicine(start_date,dosage,amount,specifications,medicament_id,patient_id,employee_id)
    VALUES (
        TO_DATE('7.11.2015', 'dd.mm.yyyy'),
        '1-2 tablety podľa potreby v časovom odstupe najmenej 4 hodiny',
        '20 tabliet',
        'maximálna denná dávka 4g účinnej látky (8 tabliet)',
        1,
        3,
        6
);

INSERT INTO prescribing_medicine(start_date,dosage,amount,specifications,medicament_id,patient_id,employee_id)
    VALUES (
        TO_DATE('15.10.2015', 'dd.mm.yyyy'),
        'dospelým 52 kvapiek 3-krát denne alebo 25 kvapiek 6-krát denne',
        '150ml balenie',
        'Užívať maximálne 7 dní',
        5,
        4,
        6
);

INSERT INTO prescribing_medicine(start_date,dosage,amount,specifications,medicament_id,patient_id,employee_id)
    VALUES (
        TO_DATE('4.9.2015', 'dd.mm.yyyy'),
        'Počiatočná liečba:3-krát denne 1 tabletu počas 8 - 12 týždňov, Dlhodobá liečba:2-krát denne 1 tabletu, Pri dementnom syndróme:3-krát denne 1-2 tablety.',
        '60 tabliet',
        'Pri podozrení na zažívacie problémy prerušiť užívanie, ak sa stav nezlepší vyhľadať lekára',
        2,
        5,
        1
);