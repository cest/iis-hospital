DROP TABLE IF EXISTS patient CASCADE;
DROP TABLE IF EXISTS employee CASCADE;
DROP TABLE IF EXISTS medicament CASCADE;
DROP TABLE IF EXISTS prescribing_medicine CASCADE;
DROP TABLE IF EXISTS hospitalization CASCADE;
DROP TABLE IF EXISTS medical_arbitration CASCADE;
DROP TABLE IF EXISTS department CASCADE;
DROP TABLE IF EXISTS work_type CASCADE;
DROP TABLE IF EXISTS work_details CASCADE;
DROP TABLE IF EXISTS users CASCADE;
DROP SEQUENCE IF EXISTS patient_id_seq CASCADE;
DROP SEQUENCE IF EXISTS employee_id_seq CASCADE;
DROP SEQUENCE IF EXISTS medicament_id_seq CASCADE;
DROP SEQUENCE IF EXISTS medical_arbitration_id_seq CASCADE;
DROP SEQUENCE IF EXISTS prescribing_medicine_id_seq CASCADE;
DROP SEQUENCE IF EXISTS hospitalization_id_seq CASCADE;


CREATE SEQUENCE patient_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE employee_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE medicament_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE prescribing_medicine_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE hospitalization_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE medical_arbitration_id_seq START WITH 1 INCREMENT BY 1;

CREATE TABLE patient(
    id             INTEGER PRIMARY KEY    DEFAULT NEXTVAL('patient_id_seq'),
    birth_number   VARCHAR(11)            NOT NULL,
    name           VARCHAR(256)           NOT NULL,
    surname        VARCHAR(256)           NOT NULL,
    date_of_birth  DATE                   NOT NULL,
    gender         VARCHAR(10)            NOT NULL,
    insurance      VARCHAR(100)           NOT NULL,
    address        VARCHAR(256)           NOT NULL,
    city           VARCHAR(256)           NOT NULL,
    zip_code       VARCHAR(10)            NOT NULL,
    phone          VARCHAR(20),
    email          VARCHAR(256)
);

CREATE TABLE department(
    id         VARCHAR(3) PRIMARY KEY,
    name       VARCHAR(256)            NOT NULL,
    floor      INTEGER                 NOT NULL,
    phone      VARCHAR(15)             NOT NULL
);

CREATE TABLE employee(
    id             INTEGER PRIMARY KEY    DEFAULT NEXTVAL('employee_id_seq'),
    birth_number   VARCHAR(11)            NOT NULL,
    name           VARCHAR(256)           NOT NULL,
    surname        VARCHAR(256)           NOT NULL,
    date_of_birth  DATE                   NOT NULL,
    gender         VARCHAR(10)            NOT NULL,
    insurance      VARCHAR(100)           NOT NULL,
    address        VARCHAR(256)           NOT NULL,
    city           VARCHAR(256)           NOT NULL,
    zip_code       VARCHAR(10)            NOT NULL,
    phone          VARCHAR(20)            NOT NULL,
    email          VARCHAR(256),
    type           VARCHAR(20)            NOT NULL,
    degree         VARCHAR(10),
    competency     VARCHAR(256),
    department_id  VARCHAR(3) REFERENCES  department(id),
    active         BOOL                   DEFAULT TRUE
);

CREATE TABLE users(
    id             INTEGER PRIMARY KEY    REFERENCES     employee(id)      NOT NULL,
    username       VARCHAR(256)           NOT NULL,
    password       VARCHAR(256)           NOT NULL
);

CREATE TABLE medicament(
    id                INTEGER PRIMARY KEY    DEFAULT NEXTVAL('medicament_id_seq'),
    name              VARCHAR(256)           NOT NULL,
    active_substance  VARCHAR(256)           NOT NULL,
    composition       VARCHAR(256)           NOT NULL,
    indications       VARCHAR(256)           NOT NULL,
    contraindications VARCHAR(256)           NOT NULL,
    form              VARCHAR(100)           NOT NULL,
    rec_dosage        VARCHAR(256)           NOT NULL,
    active            BOOL                   DEFAULT TRUE
);

CREATE TABLE prescribing_medicine(
    id              INTEGER PRIMARY KEY   DEFAULT NEXTVAL('prescribing_medicine_id_seq'),
    start_date      DATE                  NOT NULL,
    dosage          VARCHAR(256)          NOT NULL,
    amount          VARCHAR(256)          NOT NULL,
    specifications  VARCHAR(256),
    medicament_id   INTEGER REFERENCES    medicament(id)    NOT NULL,
    patient_id      INTEGER REFERENCES    patient(id)       NOT NULL,
    employee_id    INTEGER REFERENCES     employee(id)      NOT NULL
);

CREATE TABLE hospitalization(
    id             INTEGER PRIMARY KEY   DEFAULT NEXTVAL('hospitalization_id_seq'),
    start_date     DATE                   NOT NULL,
    end_date       DATE,
    room           VARCHAR(256)           NOT NULL,
    diagnosis      VARCHAR(256)           NOT NULL,
    patient_id     INTEGER REFERENCES     patient(id)       NOT NULL,
    department_id  VARCHAR(3) REFERENCES  department(id)    NOT NULL,
    employee_id    INTEGER REFERENCES     employee(id)      NOT NULL
);

CREATE TABLE medical_arbitration(
    id           INTEGER PRIMARY KEY   DEFAULT NEXTVAL('medical_arbitration_id_seq'),
    name         VARCHAR(256)          NOT NULL,
    adate        DATE                  NOT NULL,
    details      VARCHAR(256)          NOT NULL,
    patient_id   INTEGER REFERENCES    patient(id)    NOT NULL,
    employee_id  INTEGER REFERENCES    employee(id)   NOT NULL
);

CREATE TABLE work_type(
    id         VARCHAR(3) PRIMARY KEY,
    name       VARCHAR(256)            NOT NULL
);

CREATE TABLE work_details (
    doctor_id           INTEGER REFERENCES      employee(id)     NOT NULL,
    department_id       VARCHAR(3) REFERENCES   department(id)   NOT NULL,
    start_date          DATE                    NOT NULL,
    end_date            DATE                    NOT NULL,
    work_type_id        VARCHAR(3) REFERENCES   work_type(id)    NOT NULL,
    PRIMARY KEY(doctor_id, department_id)
);
